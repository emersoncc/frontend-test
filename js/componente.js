const template = document.createElement('template');
template.innerHTML = `
<style>
/* Component Style */
.cm-pais .cm-container{
    cursor: default;
    display: flex;
    flex-direction: column;
    height: 100%;
}
.row{
    margin: auto 0;
    display: flex;
    font-size: 0.85rem;
}
.row .tag-pais{
    min-width: 90px;
    color: #686868;
    font-weight: bolder;
    margin-right: 3px;
}
.row .info-pais{
    word-break: break-word;
}
.cm-container .row-one{
    font-size: 1rem;
    padding-bottom: 2px;
    margin-bottom: 4px;
    border-bottom: 1px solid #dfdfdf;
}
.cm-container .row-one .name-pais{
    cursor: pointer;
    font-weight: bold;
}
.cm-container .row-one .name-pais:hover{
    color: #1300ba;
}
.cm-container .row-one .flag-pais{
    height: 17.5px;
    margin-right: 5px;
}
</style>
<div class="cm-container">
    <div class="row row-one">
        <img class="flag-pais">
        <span class="name-pais" title="More information" onclick="toogleModal(event, 'OPEN')"></span>
    </div>
    <div class="row row-two">
        <span class="tag-pais">Official name: </span>
        <span class="info-pais official-name-pais"></span>
    </div>
    <div class="row row-tree">
        <span class="tag-pais">Capital: </span>
        <span class="info-pais capital-pais"></span>
    </div>
    <div class="row row-four">
        <span class="tag-pais">Population: </span>
        <span class="info-pais population-pais"></span>
    </div>
    <div class="row row-five">
        <span class="tag-pais">Time zone: </span>
        <span class="info-pais time-zone-pais"></span>
    </div>
    <div class="row row-six">
        <span class="tag-pais">Currencies: </span>
        <span class="info-pais currency-pais"></span>
    </div>
    <div class="row row-seven">
        <span class="tag-pais">Languages: </span>
        <span class="info-pais lang-pais"></span>
    </div>
</div>`;
class Pais extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({ mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        const datos = this.getAttribute('data-country').split("||");
        this.shadowRoot.querySelector('.name-pais').setAttribute("data-modal", this.getAttribute('data-country'));
        this.shadowRoot.querySelector('.name-pais').innerText = datos[0];
        this.shadowRoot.querySelector('.flag-pais').setAttribute("src", datos[6]);
        this.shadowRoot.querySelector('.official-name-pais').innerText = datos[1];
        this.shadowRoot.querySelector('.capital-pais').innerText = datos[2];
        this.shadowRoot.querySelector('.population-pais').innerText = datos[4];
        this.shadowRoot.querySelector('.time-zone-pais').innerText = datos[5];
        this.shadowRoot.querySelector('.currency-pais').innerText = datos[8];
        this.shadowRoot.querySelector('.lang-pais').innerText = datos[9];
    };
}

loadCountries().then((result) => {
    if (result == "OK") {
        window.customElements.define('cm-pais', Pais);
    }
}).catch((err) => {
    console.log(err);
});