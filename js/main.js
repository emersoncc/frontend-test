
//Idioma principal 
const globalLang = 'spa';

function loadCountries(){
    var promesa = new Promise((resolve, reject) => {
        // MainContent
        var mainContent = document.getElementById('main');
        mainContent.innerHTML = '';
        
        // Llamando API
        fetch('https://restcountries.com/v3.1/lang/spa').then(response => response.json()).then(data => {
            const paises = data;
            paises.forEach((pais, p) => {
                if (p < 12) {
                    //Cadenas Menores
                    var listIdiomas = Object.values(pais.languages);
                    var listMonedas = Object.values(pais.currencies);
                    var idiomas = "";
                    var monedas = "";
                    
                    listIdiomas.forEach((idioma, i) => {
                        if (i == 0) {
                            idiomas += idioma;
                        } else {
                            idiomas += ", " + idioma;
                        }
                    });
                    listMonedas.forEach((moneda, m) => {
                        if (m == 0) {
                            monedas += moneda.name + " (" + moneda.symbol + ")";
                        } else {
                            monedas += ", " + moneda.name + " (" + moneda.symbol + ")";
                        }
                    });
                    
                    //Cadena General
                    var datos = pais.name.nativeName[globalLang].common + "||" + //Nombre comun
                                pais.name.nativeName[globalLang].official + "||" + //Nombre Oficial
                                pais.capital[0] + "||" + //Capital
                                pais.continents[0] + "||" + //Continente
                                pais.population + "||" + //Población
                                pais.timezones[0] + "||" + //Zona Horaria
                                pais.flags.png + "||" + //Bandera
                                pais.coatOfArms.png + "||" + //Escudo
                                monedas + "||" + //Monedas
                                idiomas; //Idiomas
                    
                    //Asignacion de Propiedad
                    const newPais = document.createElement("cm-pais");
                    newPais.setAttribute("class", "cm-pais");
                    newPais.setAttribute("data-country", datos);
                    mainContent.append(newPais);
                }
            });
            resolve("OK");
        });
    });
    return promesa;
}

function toogleModal(e, action){
    var modal = document.getElementById("modal");
    switch (action) {
        case 'OPEN':
            const data = e.target.getAttribute('data-modal').split("||");
            document.querySelector(".modal .continent-pais").innerText = data[3];
            document.querySelector(".modal .img-flag-pais").setAttribute("src", data[6]);
            if (data[7] == "undefined") {
                document.querySelector(".modal .img-shield-pais").setAttribute("src", "img/no-image.jpg");
            } else {
                document.querySelector(".modal .img-shield-pais").setAttribute("src", data[7]);
            }
            document.querySelector(".modal .name-pais").innerText = data[0];
            document.querySelector(".modal .official-name-pais").innerText = data[1];
            document.querySelector(".modal .capital-pais").innerText = data[2];
            document.querySelector(".modal .population-pais").innerText = data[4] + " Hab.";
            document.querySelector(".modal .time-zone-pais").innerText = data[5];
            document.querySelector(".modal .currency-pais").innerText = data[8];
            document.querySelector(".modal .lang-pais").innerText = data[9];
            modal.classList.toggle("show");
        break;

        case 'CLOSE':
            modal.classList.toggle("show");
        break;
    }
}